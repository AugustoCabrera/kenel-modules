

***TRABAJO PRACTICO 4***

**Titulo:** Linux Kernel Module Programming 🎯

**Asignatura:** Sistemas de Computación 💻

**Integrantes:**
   - Cabrera, Augusto Gabriel 
   - Moroz, Esteban Mauricio 
   - Britez, Fabio
   - Gonzales, Bruno

**Fecha:** 28/5/2024
   

---------------

  <p align="center">

  
Este repositorio aborda el tema de la programación de módulos en el kernel de Linux. Se mencionan aspectos como la diferencia entre programas y módulos, la arquitectura interna de Linux, la instalación de paquetes para compilar el kernel, la descarga y compilación de ejemplos, así como la inserción y remoción de módulos. También se destaca la importancia de entender el espacio de usuario y el espacio del kernel, los riesgos asociados y la funcionalidad de los drivers en el sistema. Además, se menciona la finalización de la primera etapa del trabajo y se hace referencia a la explicación de un primer módulo. ¡Un desafío apasionante para quienes deseen adentrarse en el mundo de la programación de módulos en el kernel de Linux!

-------------
</p>

# Marco Teórico

## Kernel

El Kernel o núcleo, es una parte fundamental del sistema operativo que se encarga de conceder el acceso al hardware de forma segura para todo el software que lo solicita, el Kernel es una pequeña e invisible parte del sistema operativo, pero la más importante, ya que sin esta no podría funcionar. Todos los sistemas operativos tienen un Kernel, incluso Windows 10, pero quizá el más famoso es el Kernel de Linux, que ahora además está integrado en Windows 10 con sus últimas actualizaciones.

### ¿Para qué sirve el Kernel?

El Kernel de un sistema operativo sirve para administrar los recursos de hardware solicitados por los diferentes elementos de software y hacer de intermediario decidiendo a que y cuando se concede este acceso evitando así sobrecarga del sistema, recursos innecesarios y acceso a software malicioso al propio Kernel y llegar a poder controlar así todo el sistema. De este modo el Kernel sirve como elemento de seguridad teniendo que pasar por varias capas antes de poder tener acceso, además tiene que distribuir los recursos de manera eficiente y ordenada para que el Hardware trabaje junto al Software de la mejor manera posible.

### ¿ Módulos de Kernel ?

El kernel de Linux es modular : permite insertar y eliminar código bajo demanda con
el fin de añadir o quitar una funcionalidad. Funcionalidades que podemos añadir con
módulos:
- Drivers privativos de hardware para gráficos.
- Registrar temperaturas de componentes del ordenador.
- Hacer funcionar la tarjeta de red wifi.
- Etc.
Básicamente los módulos del kernel nos permiten hacer funcionar el hardware, y si
funciona, podemos hacer que lo haga de forma más eficiente aún.-

---------------------


# Desarrollo 

Vamos a necesitar un SO Linux instalado con sus fuentes o al menos los headers. La descarga puede demorar algunos minutos, dependiendo del BW de descarga de su conexión a internet.
Por otro lado, en esta primera parte vamos a trabajar con los siguientes programas fuentes y make files.

## Primeras Tareas

1. **Instalar build-essential y kernel-package**
   ```bash
   $ sudo apt-get install build-essential kernel-package
    ```
  Instala un conjunto de herramientas necesarias para compilar software en Linux, incluyendo el compilador gcc, make, y otros paquetes esenciales.
kernel-package: Proporciona herramientas adicionales específicas para la compilación y el manejo de paquetes del kernel.

2. **Instalar el código fuente del kernel**

    ```bash
    $ sudo apt-get install linux-source
    ```
Descarga e instala el código fuente del kernel de Linux. Esto es necesario para compilar módulos del kernel, ya que proporciona los archivos de encabezado y las fuentes necesarias.






## Comandos y Propósito en el Manejo de Módulos del Kernel en Linux


1. `Make clean`
   - **Propósito**: Elimina todos los archivos generados previamente por el proceso de compilación.
   - **Uso**: Se utiliza para asegurar que la próxima compilación comience desde cero, sin interferencias de compilaciones anteriores.

2. `Make all`
   - **Propósito**: Compila el módulo del kernel.
   - **Resultado**: Genera un archivo llamado `mimodulo.ko`, que es el módulo compilado.

   <p align="center">
  <a href="https://example.com/">
    <img src="img/image1.png" alt="Logo">
  </a>



3. `ls -l mimodulo`
   - **Propósito**: Lista todos los archivos generados con el nombre `mimodulo`.
   - **Uso**: Muestra detalles como permisos, propietario, tamaño y fecha de modificación de los archivos relacionados con `mimodulo`.


4. `modinfo mimodulo.ko`
   - **Propósito**: Proporciona información detallada sobre el módulo del kernel `mimodulo.ko`.
   - **Uso**: Muestra información como la descripción del módulo, autor, licencia, dependencias, etc.

   <p align="center">
  <a href="https://example.com/">
    <img src="img/image2.png" alt="Logo">
  </a>

5. `sudo insmod mimodulo.ko`
   - **Propósito**: Inserta el módulo `mimodulo.ko` en el kernel.
   - **Uso**: Carga el módulo en el kernel, haciendo que sus funcionalidades estén disponibles para el sistema.


 <p align="center">
  <a href="https://example.com/">
    <img src="img/image3.png" alt="Logo">
  </a>

## Desactivar Secure Boot para Resolver Problemas de Carga de Módulos

Para resolver ciertos problemas, es necesario desactivar el Secure Boot. Sigue estos pasos para hacerlo:

### 1. Accede a la Configuración del BIOS/UEFI
Busca la opción relacionada con Secure Boot. Esto puede variar dependiendo del fabricante y la versión del BIOS/UEFI, pero generalmente se encuentra en la sección de configuración de seguridad o de inicio.

### 2. Desactiva Secure Boot o Ajusta la Política
Dependiendo de tus necesidades y de las opciones disponibles en tu sistema, puedes desactivar Secure Boot por completo o ajustar la política para permitir la carga de módulos no firmados. Esto generalmente se hace cambiando el estado de la opción correspondiente de "Habilitado" a "Desactivado" o ajustando las opciones de política de Secure Boot según sea necesario.

### 3. Guarda los Cambios y Reinicia
Una vez que hayas realizado los cambios necesarios en la configuración del BIOS/UEFI, guarda los cambios y reinicia tu computadora. Esto aplicará la nueva configuración.

### 4. Verifica la Carga del Módulo
Después de desactivar Secure Boot o ajustar la política según sea necesario, intenta cargar el módulo nuevamente con `insmod` y verifica si el problema persiste. Si necesitas más ayuda o tienes alguna pregunta durante este proceso, no dudes en decirme.

 <p align="center">
  <a href="https://example.com/">
    <img src="img/image4.png" alt="Logo">
  </a>

ahora, el comando `sudo insmod mimodulo.ko` funciona: 

 <p align="center">
  <a href="https://example.com/">
    <img src="img/image5.png" alt="Logo">
  </a>

   <p align="center">
  <a href="https://example.com/">
    <img src="img/image6.png" alt="Logo">
  </a>

--------------------

6. `sudo rmmod mimodulo.ko`
   - **Propósito**: Elimina el módulo `mimodulo.ko` del kernel.
   - **Uso**: Descarga el módulo del kernel, eliminando sus funcionalidades del sistema.

7. `lsmod`
   - **Propósito**: Muestra todos los módulos actualmente cargados en el kernel.
   - **Uso**: Permite verificar si un módulo específico está cargado en el kernel.

8. `dmesg`
   - **Propósito**: Muestra el registro de mensajes del kernel.
   - **Uso**: Utilizado para ver los eventos y mensajes generados por el kernel, incluyendo aquellos generados por `printk`.



  El comando `lsmod | grep pci` muestra los módulos del kernel que están relacionados con dispositivos PCI en tu sistema.

 <p align="center">
  <a href="https://example.com/">
    <img src="img/image8.png" alt="Logo">
  </a>

### Descripción del Módulo:

- **Funcionalidad**: El módulo no realiza ninguna operación específica aparte de imprimir un mensaje al ser cargado (inicializado) utilizando la función `printk`.
- **Propósito**: La finalidad es educativa, para entender cómo insertar, eliminar y verificar la presencia de un módulo en el kernel. El uso de `printk` permite registrar mensajes en el log del kernel, útil para depuración (debugging).

## ¿De qué módulos dispone mi kernel?

### Comunicación Usuario-Kernel con `proc`:

- **Uso del comando proc**: Este comando se refiere a la interacción a través del sistema de archivos `/proc`, que es una interfaz crucial para la comunicación entre el usuario y el kernel. Permite a los módulos exponer información y aceptar comandos desde el espacio de usuario.

### Ver Módulos en el Sistema:

- `ls -l /lib/modules/$(uname -r)/kernel`
   - **Propósito**: Lista el contenido del directorio donde se almacenan los módulos del kernel para la versión actual del kernel (obtenida con `uname -r`).
   - **Uso**: Permite ver las carpetas y archivos `.ko` que contienen los módulos del kernel disponibles en el sistema.

0
 <p align="center">
  <a href="https://example.com/">
    <img src="img/image7.png" alt="Logo">
  </a>

Con este conjunto de comandos y explicaciones, puedes manejar módulos del kernel: compilarlos, cargarlos, descargarlos y verificar su estado, así como registrar mensajes para depuración y entender cómo se realiza la comunicación entre el usuario y el kernel.

### El sistema de ficheros /proc

El sistema de ficheros `/proc` es un sistema de archivos virtual que no existe físicamente en disco, sino que el kernel lo crea en memoria. Se utiliza para ofrecer información relacionada con el sistema, originalmente acerca de procesos, de ahí su nombre. Algunos de los ficheros más comunes dentro de `/proc` son:

1. **`/proc/cpuinfo`**: Este archivo proporciona información detallada sobre el procesador del sistema, incluyendo el tipo, marca, modelo, rendimiento, y más. Puedes utilizar el comando `lscpu` para mostrar la información de una manera más legible.

2. **`/proc/modules`**: Este archivo indica los módulos del núcleo que han sido cargados hasta el momento en el sistema. Puedes utilizar el comando `lsmod` para mostrar una lista de los módulos del kernel actualmente cargados.

Estos son solo algunos ejemplos de los muchos archivos disponibles en el sistema de archivos `/proc`, que proporcionan información valiosa sobre el sistema operativo y el hardware subyacente.



## Firma de módulos

Puede mejorar la seguridad de su sistema utilizando módulos del kernel firmados.

 <p align="center">
  <a href="https://example.com/">
    <img src="img/image9.png" alt="Logo">
  </a>

# Explicación de los Comandos y la Firma del Módulo del Kernel en Linux

### Primer Comando



- Lista en formato detallado los archivos en el directorio `/lib/modules/[versión_del_kernel]/kernel/crypto`, donde `[versión_del_kernel]` se reemplaza con la versión actual del kernel en ejecución.
- Filtra y muestra solo las líneas que contienen la palabra "des".

Este comando lista los archivos en el directorio especificado y muestra solo aquellos cuyo nombre contiene "des".

### Segundo Comando

`augusto@augusto-C500:~/Escritorio/Tp4_SistComp/kenel-modules/part1/module$ modinfo /lib/modules/$(uname -r)/kernel/crypto/des_generic.ko`


- Muestra información detallada sobre el módulo del kernel especificado, en este caso, `des_generic.ko`.

### Información Importante del Módulo

- **filename**: Ruta del archivo del módulo.
- **alias**: Alias que puede tener el módulo.
- **author**: Autor del módulo.
- **description**: Descripción del módulo.
- **license**: Licencia bajo la cual se distribuye el módulo.
- **srcversion**: Versión del código fuente del módulo.
- **depends**: Módulos de los que depende.
- **retpoline**: Indicador de si el módulo soporta Retpoline, una técnica de mitigación de Spectre.
- **intree**: Indicador de si el módulo está integrado en el árbol del kernel.
- **name**: Nombre del módulo.
- **vermagic**: Información de la versión del kernel para la que se compiló el módulo.
- **sig_id**: ID de la firma.
- **signer**: Quién firmó el módulo.
- **sig_key**: Clave pública utilizada para verificar la firma.
- **sig_hashalgo**: Algoritmo hash usado, en este caso, SHA-512.
- **signature**: Firma digital del módulo.

### ¿Para qué sirve la firma del módulo del kernel?

En Linux sirve para verificar la integridad y autenticidad del módulo antes de que sea cargado en el kernel. Esta práctica es parte de una serie de medidas de seguridad que ayudan a proteger el sistema contra la carga de módulos maliciosos o no autorizados. 

### Puntos clave sobre la firma de módulos:

1. Garantiza que el módulo no ha sido alterado desde que fue firmado. Si alguien modifica el módulo después de su firma, la firma se volverá inválida.

2. Permite al sistema verificar que el módulo fue creado y firmado por una fuente confiable, como el equipo de desarrollo del kernel o el mantenedor del módulo. Esto ayuda a prevenir la carga de módulos no autorizados.

3. Se reduce el riesgo de que se introduzcan en el sistema módulos maliciosos que puedan comprometer la seguridad del kernel y del sistema en general.

4. Se verifican para asegurarse de que solo se carguen módulos firmados y autorizados (en sistemas que utilizan UEFI Secure Boot). Esto es parte de un esfuerzo más amplio para asegurar que el software en el sistema sea de confianza desde el arranque hasta la operación normal.

5. La firma es más que una simple hash (la cual solo permite validar la integridad); la firma, además de garantizar la integridad, permite verificar que el módulo ha sido autenticado por la entidad distribuidora. Esto asegura que el módulo ha sido validado desde su creación hasta su integración en nuestro propio kernel.




### Pasos para Firmar y Cargar los Módulos del Kernel:

1. Autentificar un módulo del kernel.
2. Generar un par de claves públicas y privadas.
3. Importe la clave pública en el sistema de destino.
4. Firmar el módulo del kernel con la clave privada.
5. Cargar el módulo del kernel firmado.

Si el Arranque Seguro está habilitado, los cargadores de arranque del sistema operativo UEFI, el kernel de Linux y todos los módulos del kernel tienen que ser firmados con una clave privada y autenticados con la clave pública correspondiente. Si no están firmados y autenticados, el sistema no podrá terminar el proceso de arranque.


Además, el cargador de arranque de primera etapa firmado y el kernel firmado incluyen claves públicas de Red Hat incrustadas. Estos binarios ejecutables firmados y las claves incrustadas permiten que RHEL 8 se instale, arranque y ejecute con las claves de la Autoridad de Certificación de Arranque Seguro de Microsoft UEFI que son proporcionadas por el firmware UEFI en los sistemas que soportan el Arranque Seguro UEFI. Tenga en cuenta que no todos los sistemas basados en UEFI incluyen soporte para Secure Boot. 


# Desafío #1 Pt. 2

### Que es checkinstall?

Por lo general, las distribuciones de Linux usan algún sistema de gestión de paquetes. Estos están formados por una colección de herramientas diseñadas para automatizar la instalación, actualización, configuración y desinstalación de paquetes de software de forma consistente. Por lo general, estos paquetes contienen programas, librerías y datos. Además de algunos metadatos relevantes para el funcionamiento del sistema de gestión de paquetes como: nombre, descripción fabricante, suma de comprobación, versión y una lista de dependencias necesarias para que el software funcione convenientemente.

En sistemas Debian y derivados .deb es la extensión de los paquetes de software.

Estos paquetes puden ser creados a partir del codigo fuente de un proyecto.

CHECKINSTALL, es el programa que permite empaquetar ese codigo fuente de un proyecto en un paquete de extension *.rpm o .*deb.



### Empaquetar un hello world con checkinstall


Primero, asegúrate de tener instalado CheckInstall:

```sh
sudo apt-get update
sudo apt-get install checkinstall
```

Creamos un archivo de código fuente para el programa `Hello World`:

 <p align="center">
  <a href="https://example.com/">
    <img src="img/image16.png" alt="Logo">
  </a>

donde el archivo `hello.c`, contiene: 

```c
#include <stdio.h>

int main(){
printf("HELLO WORLD!");
return 0;
}
```
Compila el programa:
```sh
gcc -o hello hello.c
```

Creamos un archivo llamado `install.sh` en el directorio del proyecto `hello-world` con el siguiente contenido:


 <p align="center">
  <a href="https://example.com/">
    <img src="img/image17.png" alt="Logo">
  </a>


Una vez que el script se haya ejecutado correctamente, creamos el paquete con CheckInstall


```
augusto@augusto-C500:~/Escritorio/hello-world$ chmod +x install.sh 
augusto@augusto-C500:~/Escritorio/hello-world$ ./install.sh    
augusto@augusto-C500:~/Escritorio/hello-world$ sudo checkinstall --pkgname=hello-world --pkgversion=1.0 --backup=no --deldoc=yes --deldesc=yes --delspec=yes --default

checkinstall 1.6.3, Copyright 2010 Felipe Eduardo Sanchez Diaz Duran
           Este software es distribuído de acuerdo a la GNU GPL


The package documentation directory ./doc-pak does not exist. 
Should I create a default set of package docs?  [y]: y

Preparando la documentación del paquete...OK

* No known documentation files were found. The new package 
* won't include a documentation directory.

***************
** Debian package creation selected *
***************

Este paquete será creado de acuerdo a estos valores:

0 -  Maintainer: [ root@augusto-C500 ]
1 -  Summary: [ Package created with checkinstall 1.6.3 ]
2 -  Name:    [ hello-world ]
3 -  Version: [ 1.0 ]
4 -  Release: [ 1 ]
5 -  License: [ GPL ]
6 -  Group:   [ checkinstall ]
7 -  Architecture: [ amd64 ]
8 -  Source location: [ hello-world ]
9 -  Alternate source location: [  ]
10 - Requires: [  ]
11 - Recommends: [  ]
12 - Suggests: [  ]
13 - Provides: [ hello-world ]
14 - Conflicts: [  ]
15 - Replaces: [  ]

Introduce un número para cambiar algún dato u oprime ENTER para continuar:

Installing with make install...

====================== Resultados de la instalación  =====================
cat install.sh >install 
chmod a+x install

========================== Instalación exitosa ===========================

Some of the files created by the installation are inside the home directory: /home

You probably don't want them to be included in the package.
¿Quieres que los liste para tí? [n]: n
¿Debo excluirlos del paquete (Decir que sí es una buena idea) [n]: n

Some of the files created by the installation are inside the build
directory: /home/augusto/Escritorio/hello-world

You probably don't want them to be included in the package,
especially if they are inside your home directory.
Do you want me to list them?  [n]: n
¿Debo excluirlos del paquete (Decir que sí es una buena idea) [y]: y

Copiando los archivos al directorio temporal...OK
Stripping ELF binaries and libraries...OK
Comprimiendo las páginas de manual...OK

Creando la lista de archivos... FAILED!
Creando el paquete Debian...OK

Instalando el paquete Debian...OK
Borrando directorios temporales...OK
Borrando el directorio doc-pak...OK
Borrando el directorio temporal...OK

************************

 Done. The new package has been installed and saved to

 /home/augusto/Escritorio/hello-world/hello-world_1.0-1_amd64.deb

 You can remove it from your system anytime using: 

      dpkg -r hello-world

************************

augusto@augusto-C500:~/Escritorio/hello-world$
```

Finalmente, asi creamos con éxito un paquete Debian para el hello-world  usando checkinstall.



#### Seguridad de los modulos:
Firmar los modulos del kernel es un procedimiento muy complejo. Se deben crear certificados SSL y tener un amplio conocimiento en materia de sistemas operativos y el funcionamiento del kernel.

Pasos para firmar un modulo de kernel 

1) Se debe crear un certificado SSL, en el link provisto (https://ubuntu.com/blog/how-to-sign-things-for-secure-boot) , se observa un archivo *.cnf, que permite a openssl configurar el certificado.

```
# This definition stops the following lines choking if HOME isn't
# defined.
HOME                    = .
RANDFILE                = $ENV::HOME/.rnd 
[ req ]
distinguished_name      = req_distinguished_name
x509_extensions         = v3
string_mask             = utf8only
prompt                  = no

[ req_distinguished_name ]
countryName             = CA
stateOrProvinceName     = Quebec
localityName            = Montreal
0.organizationName      = cyphermox
commonName              = Secure Boot Signing
emailAddress            = example@example.com

[ v3 ]
subjectKeyIdentifier    = hash
authorityKeyIdentifier  = keyid:always,issuer
basicConstraints        = critical,CA:FALSE
extendedKeyUsage        = codeSigning,1.3.6.1.4.1.311.10.3.6,1.3.6.1.4.1.2312.16.1.2
nsComment               = "OpenSSL Generated Certificate"
```

2) Se deben crear las keys publicas y privadas del certificado con el comando:

```bash 
openssl req -config ./openssl.cnf \
        -new -x509 -newkey rsa:2048 \
        -nodes -days 36500 -outform DER \
        -keyout "MOK.priv" \
        -out "MOK.der"
```

3) Luego, se debe encriptar la key (enroll), esto le dirá al sistema que la key creada es efectivamente valida.
Se ejecuta el comando:

```bash
sudo mokutil --import MOK.der
```

4) Estos pasos son la iniciacion de la configuracion de las keys, lo que procede, es a firmar los modulos con distintos comandos disponibles.

Esto, es un metodo para proteger nuestro kernel, cuidando la integridad del modulo a agregar. Firmar nos garantiza que no hay cambios en el modulo desde el momento de la firma, cumpliendo con las medidas de seguridad y proteccion previamente definidas por el programador o el equipo de trabajo.

Sim embargo hay algunos metodos para proteger al kernel desde el ordenador.

Entre ellos de encuentran:

- Proteccion contra un buffer overflow

- Proteccion de zonas de memoria escenciales par el funcionamineto del sistema: la memoria principal esta en un ocupada en un porcentaje por paginas del mismo sistema operativo, una falla en la escritura de memoria principal, podria hacer que se sobreescriba sobre el codigo del SO, una problematica que el programador del kernel no puede dejar pasar por alto.

- Se podria implementar un Linux Kernel Runtime Guard, que es una proteccion en tiempo real para deteccion de intusiones externas.

- Politicas detalladas para el acceso a recursos del sistema operativo por parte de procesos o usuarios.

- Politicas de proteccion de ataques contra la memoria, es decir, proteger zonas de la memoria en las cuales se encuentran estructuras de datos sensibles, por ejemplo, se podria randomizar la ubicacion de estas estructuras por medio de un algoritmo de scheduling.



__________________


## Desafío #2: Módulos vs Programas

| Modulos                                                                 | Programas                                                                                   |
|-------------------------------------------------------------------------|---------------------------------------------------------------------------------------------|
| Un módulo comienza con la ejecución de una función `module_init()`, le dice al kernel qué funcionalidad ofrece y setea el kernel a la espera de que lo utilice cuando lo necesite. Los módulos deben tener una función de entrada y otra de salida (`module_exit()`). | Un programa comienza con una función `main()`, se ejecuta hasta la última línea de código y retorna el control al sistema operativo o a quien los convocó.                 |
| La definición de los símbolos proviene del propio kernel; las únicas funciones externas que puede utilizar un módulo son las proporcionadas por el kernel. **VER sección: archivo `/proc/kallsyms`** | Ejecutar un programa sencillo(o no) implica la activación de otras funcionalidades alojadas en diferentes librerías. A continuación, se desarrolla esta sección 👇    |


Veamos el ejemplo del codigo `part1/syscalls/ejemplo_printf.c` el cual solamente hace un printf:  llama a `printf()`, compila (con opción `-Wall` y ejecutar `strace` (`strace -tt` y `strace -c`). Se observa algunas cosas:  


 <p align="center">
  <a href="https://example.com/">
    <img src="img/image10.png" alt="Logo">
  </a>

El comando `strace` ejecuta el programa EjeSimbol bajo la supervisión de strace, que rastrea las llamadas al sistema y las señales que recibe el programa. Donde se observa: 


1. `execve("./EjeSimbol", ["./EjeSimbol"], 0x7ffea8db4b70 /* 47 vars */) = 0`: Esto indica que el programa `EjeSimbol` se ejecutó correctamente. `execve` es una llamada al sistema que ejecuta un programa.

2. `access("/etc/ld.so.preload", R_OK) = -1 ENOENT (No such file or directory)`: Esta línea indica que el sistema está intentando acceder al archivo `/etc/ld.so.preload`, que se utiliza para pre-cargar bibliotecas compartidas, pero no se encontró.

3. `openat(AT_FDCWD, "/etc/ld.so.cache", O_RDONLY|O_CLOEXEC) = 3`: Aquí el sistema está abriendo el archivo `/etc/ld.so.cache`, que contiene una caché de bibliotecas compartidas que se utilizan para acelerar el proceso de carga de programas.

4. `openat(AT_FDCWD, "/lib/x86_64-linux-gnu/libc.so.6", O_RDONLY|O_CLOEXEC) = 3`: Se está abriendo la biblioteca estándar de C (`libc.so.6`), que es fundamental para la ejecución de programas en C.

5. `write(1, "We can and we must undrestand co"..., 44We can and we must undrestand computers now\n) = 44`: Aquí el programa está escribiendo el texto "We can and we must undrestand computers now" en la salida estándar (stdout). La función `write` es una llamada al sistema que escribe datos en un descriptor de archivo.

6. `exit_group(0) = ?`: Finalmente, el programa se está cerrando exitosamente con un código de salida 0, indicando que terminó sin errores. La función `exit_group` es una llamada al sistema que finaliza la ejecución de todo el proceso.


Tambien se puede obtener de otra forma: 

 <p align="center">
  <a href="https://example.com/">
    <img src="img/image11.png" alt="Logo">
  </a>


Es increíble todo el trabajo que se realiza en el fondo cuando, a nivel de código, solo se solicita una impresión en pantalla. Esto se puede resumir con el siguiente meme:
 <p align="center">
  <a href="https://example.com/">
    <img src="img/image15.jpeg" alt="Logo">
  </a>

### archivo `/proc/kallsyms`

Es un archivo especial en sistemas operativos basados en Linux que proporciona información sobre los símbolos del kernel cargados en memoria. Estos símbolos representan las funciones y variables del kernel, y pueden ser utilizados para depurar problemas del sistema, analizar el kernel en tiempo de ejecución y realizar otras tareas de diagnóstico y desarrollo. El archivo /proc/kallsyms muestra una lista de estos símbolos junto con sus direcciones de memoria, lo que puede ser útil para entender cómo está organizado el kernel en memoria y para identificar componentes específicos del kernel cargados en el sistema.

 <p align="center">
  <a href="https://example.com/">
    <img src="img/image12.png" alt="Logo">
  </a>


### Espacio de usuario vs espacio del kernel

 <p align="center">
  <a href="https://example.com/">
    <img src="img/image13.png" alt="Logo">
  </a>



Es un diagrama de bloques de un sistema informático que ejecuta Linux. El diagrama muestra las diferentes capas del sistema, desde el hardware hasta las aplicaciones del usuario. El diagrama de bloques de un sistema informático que ejecuta Linux es una herramienta útil para comprender la arquitectura del sistema operativo. El diagrama muestra las diferentes capas del sistema, desde el hardware hasta las aplicaciones del usuario, y explica cómo fluyen los datos entre estas capas.


 - *Capa de hardware:* Esta capa representa los componentes físicos del ordenador, como la CPU, la memoria RAM, el almacenamiento y los dispositivos de entrada/salida.


 - *Capa del núcleo:* se encuentra encima de la capa de hardware. El núcleo es el software que controla el hardware y proporciona una interfaz para que las aplicaciones del usuario puedan acceder a él.


- *Capa del espacio de usuario:* se encuentra encima de la capa del núcleo. La capa del espacio de usuario contiene las aplicaciones del usuario, como los navegadores web, los editores de texto y los reproductores multimedia.



#### Flujo de datos

El flujo de datos en el sistema informático es el siguiente:

1. El usuario introduce datos a través de un dispositivo de entrada, como el teclado o el ratón.
2. Los datos se envían a la CPU.
3. La CPU procesa los datos y genera resultados.
4. Los resultados se envían a la memoria RAM.
5. Los resultados se muestran al usuario a través de un dispositivo de salida, como el monitor o la impresora.



### User Space vs Kernel Space

Así como cada proceso tiene su espacio de memoria asignado, el kernel tiene su propio espacio de memoria. Y dado que los módulos pasan a formar parte del espacio de kernel, cuando fallan, falla el kernel.

#### Name Space

Un programita en C, utiliza nombres de variables convenientes y con sentido para el lector. Ahora, si escribe rutinas que serán parte de un gran programa, cualquier variable global que tenga es parte de una comunidad de variables globales de otras personas y algunos de los nombres pueden entrar en conflicto. Cuando existen muchas variables globales que no son lo suficientemente significativas para ser distinguidas, se contamina el espacio de nombres. En grandes proyectos, se debe hacer un esfuerzo para recordar nombres reservados y encontrar formas de desarrollar un esquema para nombres y símbolos únicos de variables.

El archivo `/proc/kallsyms` contiene todos los símbolos que el kernel conoce y que, por lo tanto, son accesibles para sus módulos, ya que comparten el espacio de código del kernel.


#### Que hay en la carpeta /dev?

Los drivers del sistema se mapean en el directorio `/dev`. Al ejecutar el comando:

```bash
$ ls -l /dev
```

Con este comando obtenemos información sobre el mapeo de los drivers que se encuentran en la carpeta /dev. Utilizando un filtrado con grep, podemos observar que algunos elementos están marcados con una "c". Estos elementos muestran dos números: el número mayor y el número menor.

Estos números representan identificadores específicos:

- El número mayor indica el tipo de driver.
- El número menor identifica una instancia específica del driver, contigua al número mayor, y se asocia con el nombre de la columna final.

Por ejemplo, si vemos muchos dispositivos con el mismo número mayor (como el 7), esto significa que son instancias del driver de tipo 7. El número menor diferenciará cada una de estas instancias.

 <p align="center">
  <a href="https://example.com/">
    <img src="img/image14.jpeg" alt="Logo">
  </a>

# Respuestas a preguntas extra:


### ¿Cuales no están cargados pero están disponibles? que pasa cuando el driver de un dispositivo no está disponible. 

En Linux, si un controlador no está cargado pero está disponible, puede ser por:

1. Puede que el controlador necesario no esté instalado en el sistema. En Linux, los controladores suelen estar disponibles en forma de módulos del kernel. Si el controlador no está presente, el kernel no lo cargará automáticamente.

2. El controlador puede no ser compatible con la versión del kernel que estás utilizando.

3. Si el archivo del controlador está dañado o corrupto, el kernel puede ser incapaz de cargarlo correctamente.

Para ver los controladores disponibles en Linux y los que están cargados, usamos las herramientas `lsmod` para listar los módulos del kernel cargados actualmente y `modprobe` para cargar o descargar módulos específicos.

Si encuentras el módulo correspondiente al dispositivo que estás tratando de usar en la lista generada por `lsmod`, significa que el controlador está cargado en el sistema.


### Correr hwinfo en una pc real con hw real y agregar la url de la información de hw en el reporte. 

 Primero, `hwinfo` instalado en tu sistema. 

 ```sh
sudo apt-get install hwinfo
 ```

Una vez instalado, puedes ejecutar hwinfo desde la terminal para obtener información detallada sobre el hardware de tu sistema. 

 ```sh
hwinfo --all > informe_hw.txt
 ```

Este comando guardará la información detallada del hardware en un txt.

 <p align="center">
  <a href="https://example.com/">
    <img src="img/image18.png" alt="Logo">
  </a>


### Firmar un módulo de kernel (Agregar evidencia de la compilación, carga y descarga de su propio módulo imprimiendo el nombre del equipo en los registros del kernel). 

Primero, necesitaremos generar una clave privada y un certificado X.509 para firmar nuestro módulo del kernel. Y luego instalamos la clave privada para el sistema:

 ```sh
augusto@augusto-C500:~/Escritorio/Tp4_SistComp/kenel-modules/part1/module$ openssl req -new -x509 -newkey rsa:2048 -keyout MOK.priv -outform DER -out MOK.der -nodes -days 36500 -subj "/CN=My Key/"
...+.......+...+.........+..+....+..+....+.....+....+..+....+.........+..+...+....+..+.........+.+..............+............+...+...........
.+............+.+.........+..+................+........+....+.....+......+.+.....+.+..+...+...+.+...........+....  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++.+...+..+.........+...+...+....+.....+.+...+.....+....+..+.+.........+.
.............+...............+.+...+..................+............+..+.............+...+.....+...+.......+...+.........+.....+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++..+....
-----
augusto@augusto-C500:~/Escritorio/Tp4_SistComp/kenel-modules/part1/module$ sudo /usr/src/linux-headers-$(uname -r)/scripts/sign-file sha256 ./MOK.priv ./MOK.der $(modinfo -n mimodulo.ko) 

```

Firmamos el módulo del kernel:

```
augusto@augusto-C500:~/Escritorio/Tp4_SistComp/kenel-modules/part1/module$ sudo /usr/src/linux-headers-$(uname -r)/scripts/sign-file sha256 ./MOK.priv ./MOK.der $(modinfo -n mimodulo.ko)
```

Ahora, necesitamos compilar nuestro módulo del kernel, cargarlo en el sistema y luego descargarlo.

 <p align="center">
  <a href="https://example.com/">
    <img src="img/image19.png" alt="Logo">
  </a>

  Para agregar el nombre del equipo en los registros del kernel, podemos agregar una función al módulo del kernel que imprima este nombre. Creamos el archivo `helloEvid.c`

 <p align="center">
  <a href="https://example.com/">
    <img src="img/image20.png" alt="Logo">
  </a>

Ahora, nuevamente necesitamos compilar nuestro módulo del kernel, cargarlo en el sistema y luego descargarlo.

Para verificar que los registros del kernel contienen el mensaje impreso por nuestro módulo, podemos usar el comando `dmesg`.

### Qué es un segfault? Cómo lo maneja el kernel y cómo un programa?

Un segfault o segmentation fault (falla de segmentación en español) es un tipo de error que ocurre cuando un programa intenta acceder a una región de memoria que no tiene permiso para acceder. Este es uno de los errores más comunes y críticos en los programas escritos en lenguajes de bajo nivel como C y C++.

El kernel, maneja las situaciones en las cuales no se tiene permiso para acceder a una región de memoria a traves de tablas de páginas. 
La MMU (memory management unit) es la encargada de revisar que se tengan los permisos para acceder a las estructuras de las páginas. 

Si la entrada de pagina a la cual se esta haciendo referencia con su direccion de memoria no esta marcada con el tipo de acceso requerido (read/write/execute/...), el hardware genera un "trap", que interrumpe el flujo del hilo que esta ejecutando la tarea actual, este "trap" es lo que conocemos comunmente como "Segmentation fault".

Un programa, es avisado por medio de la señal SIGSEV de la interrupción del hilo del sistema operativo que se hace cargo de la tarea. Existe un puntero al cual el sistema operativo le indica que la dirección de memoria esta fuera de su "scope" (o alcance), o que no tiene los permisos necesarios para acceder a la memoria.

La diferencia es que, en algunos programas, es posible asignar un manejador (handler) para señales como SIGSEV de manera insegura, lo que permite continuar con la ejecución del programa o no, según se desee.


#### Qué pasa si un compañero trabajando en secure boot intenta cargar un modulo firmado por mí?

La respuesta a primera vista es la siguiente:

- Si el sistema operativo reconoce mi firma como valida, el módulo se podrá cargar sin ningun problema.

- Si el sistema no tiene cargada como valida mi firma, se rechazará el módulo que se quiere cargar al kernel.

Debo compartirle a mi compañero mi certificado SSL, es decir mi informacion de clave publica, se deberá inscribir esta clave pública en su sistema para que el módulo sea reconocido como confiable.

## Bibliografía

- [Geeknetic: ¿Qué es el Kernel y para qué sirve?](https://www.geeknetic.es/Kernel/que-es-y-para-que-sirve)
- [Red Hat Enterprise Linux 8: Managing, Monitoring, and Updating the Kernel](https://access.redhat.com/documentation/es-es/red_hat_enterprise_linux/8/html/managing_monitoring_and_updating_the_kernel/signing-kernel-modules-for-secure-boot_managing-kernel-modules)
- [Oracle Documentation: Secure Boot Module](https://docs.oracle.com/es/learn/sboot-module/#prerequisites)
- [Scaler: Understanding Linux Kernel](https://www.scaler.com/topics/understanding-linux-kernel/)
