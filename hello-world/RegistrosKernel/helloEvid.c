#include <linux/module.h>
#include <linux/kernel.h>

static char *team_name = "Cabrera_Moroz_Britez_Gonzales";

static int __init init_hello(void)
{
    printk(KERN_INFO "Hola equipo: %s\n", team_name);
    return 0;
}

static void __exit cleanup_hello(void)
{
    printk(KERN_INFO "Adiós equipo: %s\n", team_name);
}

module_init(init_hello);
module_exit(cleanup_hello);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Cabrera_Moroz_Britez_Gonzales");
MODULE_DESCRIPTION("Un simple módulo de kernel que imprime el nombre del equipo");
