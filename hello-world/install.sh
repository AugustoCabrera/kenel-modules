#!/bin/bash

# Copiar el ejecutable a /usr/local/bin
sudo cp hello /usr/local/bin

# Cambiar el propietario y los permisos del ejecutable
sudo chown root:root /usr/local/bin/hello
sudo chmod 755 /usr/local/bin/hello
